import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const MySwal = withReactContent(Swal)

export const successAlert = () => {
    return MySwal.fire({
        title: <p>Success</p>,
        icon: 'success',
        footer: 'Copyright 2021',
    })
}
export const errorAlert = () => {
    return MySwal.fire({
        title: <p>Error happened?</p>,
        icon: "error"
    })
}
export const deleteAlert = () => {
    return MySwal.fire({
        title: <p>Are you sure?</p>,
        icon: "warning",
        showCancelButton: true
    })
}
export const warningAlert = ({discountsDays, pointsPrice, normalPrice}) => {
    return MySwal.fire({
        title: <p>Warning</p>,
        text: `You are about to rent with price: ${normalPrice}. 
        ${discountsDays != null ? `You can pay for ${discountsDays} day using bonus points with total: ${pointsPrice}` : ''}`,
        footer: 'Copyright 2021',
        icon: "warning",
        showCancelButton: true,
        denyButtonText: "pay with points",
        showDenyButton: discountsDays != null,
    })
}
