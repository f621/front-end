import http from "./httpService";

export function getFilmsTypes() {
    return http.get('films/types')
}

export function storeFilm(data) {
    return http.post('films', data);
}

export function updateFilm(filmId, data) {
    return http.put(`films/${filmId}`, data);
}

export function deleteFilm(filmId) {
    return http.delete(`films/${filmId}`);
}

export function getFilms() {
    return http.get('films');
}

export function getAvailableFilms() {
    return http.get('films/available');
}

export function getAllRentings() {
    return http.get('films/renting');
}

export function calculateFilmPrice(filmId, data) {
    return http.post(`films/${filmId}/calculate`, data);
}

export function startRent(filmId, data) {
    return http.post(`films/${filmId}/rent`, data);
}

export function getMyRenting() {
    return http.get('customers/rents');
}