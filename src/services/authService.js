import httpService from "./httpService";

export const login = (data) => {
    return httpService.post('auth/login', data)
}

export const setUserData = (userData, token) => {
    localStorage.setItem('accessToken', token);
    localStorage.setItem('userData', JSON.stringify(userData));
}

export const getUserData = () => {
    return localStorage.getItem('userData') ? JSON.parse(localStorage.getItem('userData')) : null;
}
