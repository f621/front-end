import axios from "axios";
import {errorAlert} from "./sweetAlertService";

const instance = axios.create({
    baseURL: 'http://localhost:3001/',
    headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
    }
});

instance.interceptors.response.use((success) => {
    return success;
}, (rej) => {
    errorAlert();
});
export default instance;