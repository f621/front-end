import './App.css';
import {
    AppBar, Box,
    Container,
    createTheme, CssBaseline,
    Divider, Drawer, IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    ThemeProvider,
    Toolbar, Typography
} from "@mui/material";
import UserContextLayout from "./contexts/userContext";
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import Login from "./pages/Login";
import AvailableFilms from "./pages/AvailableFilms";
import Register from "./pages/Register";
import AdminPanel from "./pages/AdminPanel";
import {useState} from "react";
import MenuIcon from '@mui/icons-material/Menu';
import MyRenting from "./pages/myRenting";
import Films from "./pages/Films";
import AllRenting from "./pages/Rentings";

const drawerWidth = 240;

const theme = createTheme({});

function App(props) {
    const {window} = props;
    const [mobileOpen, setMobileOpen] = useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };
    const drawer = (
        <div>
            <Toolbar/>
            <Divider/>
            <List>
                {['Films', 'Available Films', 'My Renting', 'All Renting'].map((text, index) => (
                    <Link to={text.toLowerCase().replace(/\s/g, '')}><ListItem button key={text}>
                        <ListItemText primary={text}/>
                    </ListItem></Link>
                ))}
            </List>
        </div>
    );
    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <UserContextLayout>
            <ThemeProvider theme={theme}>
                <Router>
                    <Box sx={{display: 'flex'}}>
                        <CssBaseline/>
                        <AppBar
                            position="fixed"
                            sx={{
                                width: {sm: `calc(100% - ${drawerWidth}px)`},
                                ml: {sm: `${drawerWidth}px`},
                            }}
                        >
                            <Toolbar>
                                <IconButton
                                    color="inherit"
                                    aria-label="open drawer"
                                    edge="start"
                                    onClick={handleDrawerToggle}
                                    sx={{mr: 2, display: {sm: 'none'}}}
                                >
                                    <MenuIcon/>
                                </IconButton>
                                <Typography variant="h6" noWrap component="div">
                                    Responsive drawer
                                </Typography>
                            </Toolbar>
                        </AppBar>
                        <Box
                            component="nav"
                            sx={{width: {sm: drawerWidth}, flexShrink: {sm: 0}}}
                            aria-label="mailbox folders"
                        >
                            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                            <Drawer
                                container={container}
                                variant="temporary"
                                open={mobileOpen}
                                onClose={handleDrawerToggle}
                                ModalProps={{
                                    keepMounted: true, // Better open performance on mobile.
                                }}
                                sx={{
                                    display: {xs: 'block', sm: 'none'},
                                    '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                                }}
                            >
                                {drawer}
                            </Drawer>
                            <Drawer
                                variant="permanent"
                                sx={{
                                    display: {xs: 'none', sm: 'block'},
                                    '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                                }}
                                open
                            >
                                {drawer}
                            </Drawer>
                        </Box>
                        <Box component="main" sx={{flexGrow: 1}}>

                            <main style={{backgroundColor: "#dbdcdc"}}>
                                <Container>
                                    <Switch>
                                        <Route path="/login">
                                            <Login/>
                                        </Route>
                                        <Route path="/register">
                                            <Register/>
                                        </Route>
                                        <Route path="/availablefilms">
                                            <AvailableFilms/>
                                        </Route>
                                        <Route path="/films">
                                            <Films/>
                                        </Route>
                                        <Route path="/myrenting">
                                            <MyRenting/>
                                        </Route>
                                        <Route path="/allrenting">
                                            <AllRenting/>
                                        </Route>
                                        <Route path="*">
                                            <AvailableFilms/>
                                        </Route>
                                    </Switch>
                                </Container>
                            </main>


                        </Box>
                    </Box>
                </Router>
            </ThemeProvider>
        </UserContextLayout>

    )
        ;
}

export default App;
