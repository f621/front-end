import React, {useEffect} from 'react';
import {Button, Paper, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";

const Table = ({rows = [], columns = [], keys = []}) => {
    useEffect(() => {
    console.log(columns[0]);

    }, [])
    return (
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 650}} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        {columns.length != 0  && <TableCell>{columns[0]}</TableCell>}
                        {columns.slice(1).map(col => (
                            <TableCell align="right">{col}</TableCell>
                        ))}
                        {/*<TableCell align="right">{col}</TableCell>*/}
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow
                            key={row.id}
                            sx={{'&:last-child td, &:last-child th': {border: 0}}}
                        >
                            <TableCell component="th" scope="row">
                                {row[keys[0]]}
                            </TableCell>
                            {keys.slice(1).map(k => (
                                <TableCell align="right">{row[k]}</TableCell>
                            ))}
                            {/*<TableCell align="right"><Button*/}
                            {/*    onClick={() => openCalculateModal(film.id)}>price</Button></TableCell>*/}
                        </TableRow>

                    ))}

                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default Table;