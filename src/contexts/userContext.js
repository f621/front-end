import {createContext, useState} from "react";
import {getUserData} from "../services/authService";

export const UserContext = createContext({
    userData: null,
    updateUserDate: function () {
    }
});

const UserContextLayout = (props) => {
    const [userData, setUserData] = useState(getUserData());

    const context = {
        userData: userData,
        updateUserDate: function (userData) {
            setUserData(userData)
        }
    };
    return (
        <UserContext.Provider value={context}>
            {props.children}
        </UserContext.Provider>
    );
};

export default UserContextLayout;