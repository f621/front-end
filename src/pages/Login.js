import React, {useContext} from 'react';
import styles from '../styles/login.module.css';
import {Button, Grid, Paper, TextField} from "@mui/material";
import {useForm} from "react-hook-form";
import {login, setUserData} from "../services/authService";
import {UserContext} from "../contexts/userContext";
import { useHistory } from "react-router-dom";

const Login = () => {
    const  history = useHistory();
    const userContext = useContext(UserContext)
    const {register, handleSubmit, watch, formState: {errors}} = useForm();
    const onSubmit = data => {
        login(data).then(response => {
            if (response){
                alert('success');
                setUserData(response.data.userData, response.data.accessToken)
                userContext.updateUserDate(response.data.userData);
                history.push('/')
            }
        });
    }

    return (
        <div className={styles.container}>
            <Paper elevation={3}>
                <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                    <Grid container style={{height: "inherit"}}
                          spacing={2} direction={"column"}
                          justifyContent={"center"} alignItems={"center"}>
                        <Grid item style={{paddingRight: "10rem"}}>
                            Login Form
                        </Grid>
                        <Grid item>
                            <TextField
                                {...register("username", {required: true})}
                                error={errors.email}
                                label="Username" variant="outlined"/>
                        </Grid>
                        <Grid item>
                            <TextField {...register("password", {required: "wrong"})}
                                       error={errors.password}
                                       label="Password" variant="outlined"/>
                        </Grid>
                        <Grid item>
                            <Button type={"submit"} variant={"contained"}>Submit</Button>
                        </Grid>
                    </Grid>

                </form>
            </Paper>
        </div>
    );
};

export default Login;