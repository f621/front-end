import React, {useContext, useEffect, useState} from 'react';
import {UserContext} from "../contexts/userContext";
import {useHistory} from "react-router-dom";
import styles from '../styles/films.module.css';
import {
    Button,
    Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField
} from "@mui/material";
import {calculateFilmPrice, getAllRentings, getAvailableFilms, startRent} from "../services/filmsService";
import {useForm} from "react-hook-form";
import {warningAlert} from "../services/sweetAlertService";

const AllRenting = () => {
    let router = useHistory();
    const userContext = useContext(UserContext);
    const [films, setFilms] = useState([]);
    const [total, setTotal] = useState(0)
    const getAlFilms = () => {
        getAllRentings().then(response => {
            if (response) {
                setFilms(response.data);
                const final = response.data.reduce((prev, curr) => {
                    const netTotal = curr.customerFilms.reduce((prev, curr) => {
                        return prev + curr.netTotal;
                    }, 0)
                    return prev + netTotal;
                }, 0)
                setTotal(final)
            }
        })
    }
    useEffect(() => {
        if (!userContext.userData) {
            router.push('login')
        }
        getAlFilms()
    }, [])
    return (
        <>
            <div className={styles.container}>
                <Paper elevation={2}>
                    <Container className={styles.table}>
                        <Grid container direction={"column"} spacing={2}>
                            <Grid item>
                                <h1>All Renting Operations</h1>
                                <h6>Total Payments {total}</h6>
                            </Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{minWidth: 650}} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Movie Name</TableCell>
                                                <TableCell align="right">Customer Name</TableCell>
                                                <TableCell align="center">Start Date</TableCell>
                                                <TableCell align="center">Number of days</TableCell>
                                                <TableCell align="center">Number of days Payed By Points</TableCell>
                                                <TableCell align="right">Price</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {films.map(rent => {
                                                const result = []
                                                rent.customerFilms.map(CF => {
                                                    result.push((
                                                        <TableRow
                                                            key={CF.id}
                                                            sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                                        >
                                                            <TableCell component="th" scope="row">
                                                                {rent.name}
                                                            </TableCell>
                                                            <TableCell align="right">{CF.user.name}</TableCell>
                                                            <TableCell align="center">{CF.startDate}</TableCell>
                                                            <TableCell align="center">{CF.numberOfDays}</TableCell>
                                                            <TableCell align="center">{CF.numberOfDaysPayedWithPoints??''}</TableCell>
                                                            <TableCell align="right">{CF.netTotal}</TableCell>
                                                        </TableRow>
                                                    ));
                                                })
                                                return result;
                                            })}
                                            {/*{films.map((film) => (*/}
                                            {/*    <TableRow*/}
                                            {/*        key={film.id}*/}
                                            {/*        sx={{'&:last-child td, &:last-child th': {border: 0}}}*/}
                                            {/*    >*/}
                                            {/*        <TableCell component="th" scope="row">*/}
                                            {/*            {film.name}*/}
                                            {/*        </TableCell>*/}
                                            {/*        <TableCell align="right">{film.rate}</TableCell>*/}
                                            {/*        <TableCell align="right">{film.filmType.name}</TableCell>*/}
                                            {/*        <TableCell align="right"><Button*/}
                                            {/*            onClick={() => openCalculateModal(film.id)}>price</Button></TableCell>*/}
                                            {/*    </TableRow>*/}

                                            {/*))}*/}

                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>


                    </Container>
                </Paper>
            </div>

        </>
    );
};

export default AllRenting;