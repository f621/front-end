import React, {useEffect, useState} from 'react';
import styles from '../styles/films.module.css'
import {
    Button,
    Container,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from "@mui/material";
import {getMyRenting} from "../services/filmsService";

const MyRenting = () => {
    const [renting, setRenting] = useState([]);
    const [total, setTotal] = useState(0)

    useEffect(() => {
        getMyRenting().then(response => {
            if (response)
                setRenting(response.data.customerFilms)
            const final = response.data.customerFilms.reduce((prev, curr) => {
                return prev + curr.netTotal;
            }, 0)
            setTotal(final)
        });
    })
    return (
        <div className={styles.container}>
            <Paper variant={3}>
                <Container className={styles.table}>
                    <Grid container direction={"column"} spacing={2}>
                        <Grid item>
                            <h1>My renting</h1>
                            <h6>Total payments {total}</h6>
                        </Grid>
                        <Grid item>
                            <TableContainer component={Paper}>
                                <Table sx={{minWidth: 650}} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Movie Name</TableCell>
                                            <TableCell align="right">Price</TableCell>
                                            <TableCell align="right">Days</TableCell>
                                            <TableCell align="center">Days Payed By Points</TableCell>
                                            <TableCell align="center">Start Date</TableCell>
                                            <TableCell align="right">Rate</TableCell>
                                            <TableCell align="right">Type</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody
                                        >
                                        {renting.map((rent) => (
                                            <TableRow
                                                key={rent.id}
                                                sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {rent.film.name}
                                                </TableCell>
                                                <TableCell align="right">{rent.netTotal}</TableCell>
                                                <TableCell align="center">{rent.numberOfDays}</TableCell>
                                                <TableCell align="center">{rent.numberOfDaysPayedWithPoints}</TableCell>
                                                <TableCell align="right">{rent.startDate}</TableCell>
                                                <TableCell align="right">{rent.film.rate}</TableCell>
                                                <TableCell align="right">{rent.film.filmType.name}</TableCell>
                                            </TableRow>

                                        ))}

                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </Grid>
                </Container>
            </Paper>
        </div>
    );
};

export default MyRenting;