import React, {useContext, useEffect, useState} from 'react';
import {UserContext} from "../contexts/userContext";
import {useHistory} from "react-router-dom";
import styles from '../styles/films.module.css';
import {
    Button,
    Container,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Grid, MenuItem,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField
} from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';
import {deleteFilm, getFilms, getFilmsTypes, storeFilm, updateFilm} from "../services/filmsService";
import {useForm} from "react-hook-form";
import {deleteAlert} from "../services/sweetAlertService";

const Films = () => {
    let router = useHistory();
    const userContext = useContext(UserContext);
    const {register, handleSubmit, watch, formState: {errors}} = useForm();
    const [open, setOpen] = useState(false);
    const [dialogHeader, setDialogHeader] = useState('')
    const [filmTypes, setFilmTypes] = useState([]);
    const [formType, setFormType] = useState(1);
    const [films, setFilms] = useState([]);
    const [selectedFilm, setSelectedFilm] = useState(null)


    const getAFilms = () => {
        getFilms().then(response => {
            if (response) {
                setFilms(response.data);
            }
        })
    }
    const onSubmit = data => {
        if (selectedFilm)
            updateFilm(selectedFilm.id, data).then(response => {
                if (response) {
                    handleClose()
                    getAFilms()
                }
            });
        else
            storeFilm(data).then(response => {
                if (response) {
                    handleClose()
                    getAFilms();
                }
            });
    };
    const openCreationFilm = () => {
        setSelectedFilm(null);
        setFormType(1)
        setOpen(true);
        setDialogHeader('Create New Film');
    };
    const handleUpdate = (film) => {
        setSelectedFilm(film)
        setFormType(2)
        setOpen(true);
        setDialogHeader('Update Film');
    }
    const handleClose = () => {
        setOpen(false);
    };
    const handleDelete = (filmId) => {
        deleteAlert().then(result => {
            if(result.isConfirmed){
                deleteFilm(filmId).then(response => {
                    if (response) {
                        getAFilms();
                    }
                })
            }
        });
    }

    useEffect(() => {
        if (!userContext.userData) {
            router.push('login')
        }
        getAFilms()
        getFilmsTypes().then(response => {
            if (response) {
                setFilmTypes(response.data)
            }
        });
    }, [selectedFilm])
    return (
        <>
            <div className={styles.container}>
                <Paper elevation={2}>
                    <Container className={styles.table}>
                        <Grid container direction={"column"} spacing={2}>
                            <Grid item>
                                <h1>All Films</h1>
                                <div>
                                    <Button onClick={openCreationFilm}
                                            variant={"contained"} style={{textTransform: "capitalize"}}>
                                        Create
                                    </Button>
                                </div>
                            </Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{minWidth: 650}} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Name</TableCell>
                                                <TableCell align="right">Rate</TableCell>
                                                <TableCell align="right">Type</TableCell>
                                                <TableCell align="right">Actions</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {films.map((film) => (
                                                <TableRow
                                                    key={film.id}
                                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                                >
                                                    <TableCell component="th" scope="row">
                                                        {film.name}
                                                    </TableCell>
                                                    <TableCell align="right">{film.rate}</TableCell>
                                                    <TableCell align="right">{film.filmType.name}</TableCell>
                                                    <TableCell align="right">
                                                        <Button
                                                            onClick={() => handleUpdate(film)}>Update</Button>
                                                        <Button
                                                            onClick={() => handleDelete(film.id)}>
                                                            <DeleteIcon/>
                                                        </Button>

                                                    </TableCell>
                                                </TableRow>

                                            ))}

                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>


                    </Container>
                </Paper>
            </div>

            <Dialog
                style={{zIndex: 1000}}
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogTitle id="alert-dialog-title">
                        {dialogHeader}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <div style={{height: "11rem", width: "30rem"}} className={styles.normalContainer}>
                                <Grid container spacing={3}>
                                    <Grid item xs={6} hidden={formType === 2}>
                                        <TextField id="name"
                                                   label="Name"
                                                   defaultValue={selectedFilm?.name}
                                                   variant="outlined"
                                                   error={errors.name}
                                                   {...register("name", {required: true})}
                                        />
                                    </Grid>
                                    <Grid item xs={6} hidden={formType === 2}>
                                        <TextField id="rate"
                                                   label="Rate"
                                                   variant="outlined"
                                                   defaultValue={selectedFilm?.rate}
                                                   error={errors.rate}
                                                   {...register("rate", {required: true})}
                                        />
                                    </Grid>
                                    <Grid item xs={6}>
                                        <TextField
                                            id="filmTypeId"
                                            select
                                            defaultValue={selectedFilm?.filmTypeId}
                                            error={errors.filmTypeId}
                                            style={{width: "100%"}}
                                            label="Select"
                                            {...register("filmTypeId", {required: true})}
                                        >
                                            {filmTypes.map(type => (
                                                <MenuItem key={type.id} value={type.id}>
                                                    {type.name}
                                                </MenuItem>
                                            ))}
                                        </TextField>
                                    </Grid>
                                </Grid>

                            </div>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button type={"submit"} variant={"contained"} style={{margin: "1rem"}} autoFocus>
                            Submit
                        </Button>
                        <Button color={"error"} style={{margin: "1rem"}}
                                onClick={handleClose}>
                            Close
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </>
    );
};

export default Films;