import React, {useContext, useEffect, useState} from 'react';
import {UserContext} from "../contexts/userContext";
import {useHistory} from "react-router-dom";
import styles from '../styles/films.module.css';
import {
    Button,
    Container, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Grid,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow, TextField
} from "@mui/material";
import {calculateFilmPrice, getAvailableFilms, startRent} from "../services/filmsService";
import {useForm} from "react-hook-form";
import {warningAlert} from "../services/sweetAlertService";

const AvailableFilms = () => {
    let router = useHistory();
    const userContext = useContext(UserContext);
    const {register, handleSubmit, watch, formState: {errors}} = useForm();
    const [open, setOpen] = useState(false);
    const [films, setFilms] = useState([]);
    const [selectedFilmId, setSelectedFilmId] = useState(null)


    const getAFilms = () => {
        getAvailableFilms().then(response => {
            if (response) {
                setFilms(response.data);
            }
        })
    }
    const onSubmit = data => {
        calculateFilmPrice(selectedFilmId, data).then(response => {
            if (response) {
                setOpen(false);
                warningAlert(response.data).then(result => {
                    if (result.isConfirmed) {
                        startRent(selectedFilmId, {...data, points: false}).then(response => {
                            if (response) {
                                getAFilms();
                            }
                        });
                    } else if (result.isDenied) {
                        startRent(selectedFilmId, {...data, points: true}).then(response => {
                            if (response) {
                                getAFilms();
                            }
                        });
                    }
                });
            }
        });
    };
    const openCalculateModal = (filmId) => {
        setOpen(true);
        setSelectedFilmId(filmId)
    };
    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        if (!userContext.userData) {
            router.push('login')
        }
        getAFilms()
    }, [])
    return (
        <>
            <div className={styles.container}>
                <Paper elevation={2}>
                    <Container className={styles.table}>
                        <Grid container direction={"column"} spacing={2}>
                            <Grid item>
                                <h1>Available Films</h1>
                            </Grid>
                            <Grid item>
                                <TableContainer component={Paper}>
                                    <Table sx={{minWidth: 650}} aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Name</TableCell>
                                                <TableCell align="right">Rate</TableCell>
                                                <TableCell align="right">Type</TableCell>
                                                <TableCell align="right">Action</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {films.map((film) => (
                                                <TableRow
                                                    key={film.id}
                                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                                >
                                                    <TableCell component="th" scope="row">
                                                        {film.name}
                                                    </TableCell>
                                                    <TableCell align="right">{film.rate}</TableCell>
                                                    <TableCell align="right">{film.filmType.name}</TableCell>
                                                    <TableCell align="right"><Button
                                                        onClick={() => openCalculateModal(film.id)}>price</Button></TableCell>
                                                </TableRow>

                                            ))}

                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                        </Grid>


                    </Container>
                </Paper>
            </div>

            <Dialog
                style={{zIndex: 1000}}
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <form onSubmit={handleSubmit(onSubmit)}>
                    <DialogTitle id="alert-dialog-title">
                        {"Calculate rental price"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            <div style={{height: "5rem", width: "30rem"}} className={styles.normalContainer}>
                                <TextField id="days"
                                           type={"number"}
                                           label="Number of renting days"
                                           variant="outlined"
                                           error={errors.days}
                                           {...register("days", {required: true})}
                                />
                            </div>
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button type={"submit"} style={{margin: "1rem"}} autoFocus>
                            Calculate
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </>
    );
};

export default AvailableFilms;